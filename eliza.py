# Let's get this party started
import falcon
import json
from rq import Queue
from redis import Redis
from task import healthcheck_endpoint
from settings import POST_URL


import logging
logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename='myapp.log',
                        filemode='w')

console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)

LOG = logging.getLogger('eliza')



def get_conn():
    return Redis()


# Falcon follows the REST architectural style, meaning (among
# other things) that you think in terms of resources and state
# transitions, which map to HTTP verbs.
class ThingsResource:
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ('\nTwo things awe me most, the starry sky '
                     'above me and the moral law within me.\n'
                     '\n'
                     '    ~ Immanuel Kant\n\n')

class PingResource:
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.set_header('Content-Type', 'application/json')
        resp.body = ('{"ping":"pong"}')


class HealthChecksResource:
    def on_post(self, req, resp):
        try:
                raw_json = req.stream.read()
        except Exception as ex:
                raise falcon.HTTPError(falcon.HTTP_400,
                    'Error',
                    ex.message)

        try:
            rj = json.loads(raw_json, encoding='utf-8')
        except ValueError:
            raise falcon.HTTPError(falcon.HTTP_400,
                'Malformed JSON',
                'Could not decode the request body. The '
                'JSON was incorrect.')
        try:
            endpoint = rj['endpoint']
            max_retries = rj.get('max_retries', 3)
            internet_addr = rj.get('internet_addr', 'http://www.google.com')
            request_id = rj['request_id']


            q = Queue('healthcheck', connection=get_conn())
            q.enqueue_call(func=healthcheck_endpoint,
                            args=(endpoint, max_retries, internet_addr,
                                  request_id, POST_URL))
        except KeyError:
            raise falcon.HTTPError(falcon.HTTP_400,
                'Missing parameters')

            resp.status = falcon.HTTP_202
            resp.body = json.dumps(rj, encoding='utf-8')


# falcon.API instances are callable WSGI apps
app = falcon.API()

# Resources are represented by long-lived class instances
things = ThingsResource()
ping = PingResource()
healthchecks = HealthChecksResource()

# things will handle all requests to the '/things' URL path
app.add_route('/ping', ping)
app.add_route('/healthchecks', healthchecks)
