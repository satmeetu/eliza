import requests
import json
from settings import RESP_CODES
import logging
LOG = logging.getLogger(__name__)


def healthcheck_endpoint(url, retries, other_url, request_id, post_url):
    """ Check the endpoint (ping) for the number of retries.
        Also check the other_url if 'url' is not accessible. To
        eliminate false positives
    """
    current_try = 0
    error_state = -1
    while current_try < retries:
        current_try += 1
        try:
            r = requests.get(url)
            if r.status_code in [200, 201, 202]:
                error_state = 0
                break
        except:
            LOG.info("current try %d (%s)failed" % (current_try, url))
            print "current try %d (%s)failed" % (current_try, url)

    if error_state == -1: #url cannot be reached, try the other_url

        try:
            r = requests.get(other_url)
            if r.status_code in [200, 201, 202]:
                LOG.info("Success connecting to other url %s" % (other_url))
                error_state = 1
            else:
                error_state = 2
        except:
            LOG.info("Cannot connect to other url %s either" % (other_url))
            error_state = 2

    # POST results to the url provided
    d = dict(request_id=request_id, status=RESP_CODES[error_state])
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    try:
        r =  requests.post(post_url, data=json.dumps(d), headers=headers)
    except Exception as e:
        LOG.info("Error posting result %s " % str(e))


